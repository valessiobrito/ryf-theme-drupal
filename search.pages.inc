<?php


/* implements template_preprocess_search_result */

function ryf_preprocess_search_result (&$variables) {

	//debug(unserialize(serialize($variables)));
	//debug(unserialize(serialize($variables['result']['node'])));
	//debug(unserialize(serialize($variables['result']['node']->toArray()['field_image'])));

	// this attaches the 'node' object so that we can access the image in field_image, via our twig template.
	$variables['node'] = $variables['result']['node'];

}

