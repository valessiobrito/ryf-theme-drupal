<?php
/**
 * @file
 * Contains \Drupal\footer\Plugin\Block\FooterBlock.
 */
namespace Drupal\footer\Plugin\Block;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a 'footer' block.
 *
 * @Block(
 *   id = "footer_block",
 *   admin_label = @Translation("Footer"),
 *   category = @Translation("RYF Custom Module")
 * )
 */
class FooterBlock extends BlockBase implements BlockPluginInterface {
  /**
   * {@inheritdoc}
   */ 
    public function build() {

      return array(
        '#theme' => 'footer',
        '#attached' => array(
          'library' => array(
            'footer/footer',
          ),
        ),
      );

    }
}
